---
layout: page
title: About
permalink: /about
---

This is my [Digital Garden](https://joelhooks.com/digital-garden), an extension of my blog at <https://www.ecliptik.com.>

It is a place to grow ideas, notes, and other things that don't warrant a full blog post but I still want available online.

Tools Used:

- [Obsidian](https://obsidian.md)
- [Jekyll](https://jekyllrb.com)
- [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)
- [iCloud](https://help.obsidian.md/Obsidian/iOS+app)

Template originally from: <https://github.com/maximevaillancourt/digital-garden-jekyll-template>
