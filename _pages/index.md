---
layout: page
title: Home
id: home
permalink: /
---

## Featured Seeds
- [Digital Gardens](/digital-gardens)

## All Seeds
<ul>
  {% for note in site.notes %}
    <li>
      <a href="{{ note.url }}">{{ note.title }}</a>
    </li>
  {% endfor %}
</ul>

<style>
  .wrapper {
    max-width: 46em;
  }
</style>
