---
title: Digital Gardens
tags: digitalgarden garden stream
date: 2021-11-01
---

>For mature thought there is no mechanical substitute. But creative thought and essentially repetitive thought are very different things. For the latter there are, and may be, powerful mechanical aids.

### Concepts

**Garden**
- web as space
- Every walk through thr garden creates new meanings
- When adding things to the garden allows for future unpredictable relationships
- No one "correct" approach, vistors create the meaning, meaning sill evolve over time
- Not a time bound relationship
- Relationship is to the garden has a while, but no direct relationships, allowing to grow into something bigger than a singular narratice

**Streams**
- get in the stream and things flow around you
- Not always passive, can add to stream
- Stream if meaning, replces topology with serialization
- Interpret things in a stream in how they've occurred
- Linked to context to understand meaning
- Self-assertive
- Twitter, blogging, "persona"


![](assets/E6CCBB64-F3B7-4DCE-BD7E-ABA115720AA6.jpeg)

### Workflows

- <https://refinedmind.co/my-digital-ecosystem>


### Building Digital Gardens

- [My blog is a digital garden, not a blog](https://joelhooks.com/digital-garden)
- [How to set up your own digital garden](https://nesslabs.com/digital-garden-set-up)
- [Obsidian Jekyll workflow](https://refinedmind.co/obsidian-jekyll-workflow)
- <https://refinedmind.co/digital-garden>
- <https://wildrye.com/how-to-design-your-digital-garden/>

### Essays

- [As We May Think](https://web.archive.org/web/20210228110736/https://www.theatlantic.com/magazine/archive/1945/07/as-we-may-think/303881/)
- <https://egghead.io/lessons/egghead-sector-the-future-of-mdx-and-digital-gardens>
- <https://tomcritchlow.com/2019/02/17/building-digital-garden/>
- <https://maggieappleton.com/garden-history>

### Videos

- <https://youtu.be/ckv_CjyKyZY>
- <https://archive.org/details/gardens-and-streams-wikis-blogs-and-ui-popup-indie-web-camp-session-2020/GardensAndStreamsWikisBlogsAndUI-SpeakerView-PopupIndieWebCampSession-2020.mp4>

### Gardens
- <https://maggieappleton.com/garden>