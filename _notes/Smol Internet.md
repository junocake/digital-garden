---
title: Smol Internet
tags: smallweb gemini fediverse cozyweb
date: 2021-11-03
---

>To use the smol web is not to reject the wide one. It’s just a different space moving at a different pace.

## Essays

- <https://thedorkweb.substack.com/p/gopher-gemini-and-the-smol-internet>
- <https://studio.ribbonfarm.com/p/the-extended-internet-universe>
- https://thedorkweb.substack.com/p/escaping-the-webs-dark-forest

## Protocols

## Messaging

- <https://scuttlebutt.nz/get-started/>