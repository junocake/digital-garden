---
title: Obsidian iCloud Sync
tags: obsidian icloud notes
date: 2021-11-01
---

Using [[Obsidian]] to managed a [[Digital Gardens]]

- Create new Vault and git repot on MacBook
- Setup Obsidian app on iPhone
    - Create new vault on iCloud storage with same name as one on MacBook
- Copy the Vault from the MacBook to
- 
```
~/Library/Mobile\ Documents/iCloud\~md\~obsidian/Documents
```

The vault and entire Digital Garden git site will then show up in Obsidian iCloud storage,

![](assets/Pasted%20image%2020211030215547.png)

Saving files on MacBook, iPhone, and any other iCloud connected device will update the Vault.

Doing a git add/commit/push will then publish to garden.ecliptik.com on Gitlab pages.

## References

- <https://osxdaily.com/2017/11/16/access-icloud-drive-command-line-mac/>
- <https://www.digitalocean.com/community/tutorials/markdown-markdown-images>
- <https://raw.githubusercontent.com/maximevaillancourt/digital-garden-jekyll-template/master/_notes/your-first-note.md>