---
title: Digital Wellness
tags: health wellness
date: 2021-11-05
---

## Links

- <https://datagubbe.se/sage/>
- <https://spectrum.ieee.org/disobeying-your-phone>

## References

- [Working in the Garage](Working%20in%20the%20Garage.md)