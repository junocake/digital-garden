---
title: Debian
tags: debian linux unix
date: 2021-11-07
---

## Encrypted HDD

### Create properly aligned partition

```
sudo parted -a optimal /dev/sda
mklabel gpt
```

```
sudo parted -a optimal /dev/sda mkpart primary '0%' '100%'
```
### Created encrypted partition

```
sudo cryptsetup luksFormat /dev/sda1
```

### Add new key on USB drive

```
sudo cryptsetup luksAddKey /dev/sda1 /root/mnt/jezebelVG-key
```
  
### Open encrypted volume with key

```
sudo cryptsetup luksOpen /dev/sda1 new_home --key-file=/root/mnt/jezebelVG-key
```

### Format encrypted partition

```
sudo mkfs.ext4 /dev/mapper/new_home
```

### Set reserved blocks to 0%**

```
sudo tune2fs -r 0 /dev/mapper/new_home
```
### Add to /etc/crypttab

```
home_decrypt /dev/disk/by-partuuid/b4150d08-6000-42f3-b9bf-4a9fbfe358e3 /root/mnt/jezebelVG-key
```
  

### Add to /etc/fstab

```
/dev/mapper/home_decrypt /home ext4 defaults,journal_checksum,user_xattr,x-systemd.requires-mounts-for=/root/mnt,nofail,x-systemd.device-timeout=10s 0 2
```