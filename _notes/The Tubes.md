---
title: The Tubes
tags: tubes smol 
date: 2021-11-07
---

## The Tubes

>[#theTubes](https://toot-lab.reclaim.technology/tags/theTubes) is an ad hoc experiment around throwing together a decentralized peer-to-peer shared vpn fabric amongst a small group of node operators who have some common interest and connection.

See <https://toot-lab.reclaim.technology/@djsundog/105886194663952597>

Can be considered part of [Smol Internet](Smol%20Internet.md)

### Docs

[https://pvpn.reclaim.technology/wg-portal_notes](https://pvpn.reclaim.technology/wg-portal_notes)

[https://lipidity.com/openbsd/wireguard/](https://lipidity.com/openbsd/wireguard/)

[https://thomasward.com/openbsd-wireguard/](https://thomasward.com/openbsd-wireguard/)

[https://toot-lab.reclaim.technology/@djsundog/105886194663952597](https://toot-lab.reclaim.technology/@djsundog/105886194663952597)

the next open subnet was 10.42.14.0/24, so that's all yours

once you've got your setup going the way you'd like and you're ready to peer, shoot me a PublicKey (can be the same keys you use on wg0 or you can use a separate set) and I'll set up a peering endpoint and send back a config file for wg1.conf on your end - then you'll add your PrivateKey to that config and bring up wg1 and you'll be in!

Let me know if you run into any issues I can help with :)

### Install Go on OpenBSD 6.8

#### Install 1.16 from snapshot

OpenBSD 6.8 ships with go v1.15.2, wg-portal however requires go v1.16, which is available in SNAPSHOT.

[https://cdn.openbsd.org/pub/OpenBSD/snapshots/packages/aarch64/go-1.16.2.tgz](https://cdn.openbsd.org/pub/OpenBSD/snapshots/packages/aarch64/go-1.16.2.tgz)

doas pkg_add go-1.16.2.tgz

#### Build **WireGuard Portal**

wget [https://github.com/h44z/wg-portal/archive/refs/tags/v1.0.3.tar.gz](https://github.com/h44z/wg-portal/archive/refs/tags/v1.0.3.tar.gz)

```
tar -xvzf v1.0.3.tar.gz
cd wg-portal-1.0.3
make dep
make build-cross-plat
```

#### Setup Wireguard