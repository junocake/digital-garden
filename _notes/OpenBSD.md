---
title: OpenBSD
tags: openbsd bsd unix os
date: 2021-11-07
---

## Peformance

[https://klarasystems.com/articles/freebsd-performance-observability/](https://klarasystems.com/articles/freebsd-performance-observability/)

## Useful Commands
- search packages:
`pkg_info -Q string`