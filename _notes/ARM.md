---
title: ARM
tags: arm graviton aws
date: 2021-11-08
---

## Links

- <https://github.com/aws/aws-graviton-getting-started/blob/main/containers.md>
- <https://github.com/aws/aws-graviton-getting-started/blob/main/managed_services.md>
- <https://www.docker.com/blog/speed-up-building-with-docker-buildx-and-graviton2-ec2/>